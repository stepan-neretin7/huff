import argparse
import queue
from collections import Counter
from pprint import pprint

from node import Node

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description='Parsing command-line flags and arguments')

# Add the required flags
parser.add_argument('-c', action='store_true', help='Enable flag -c')
parser.add_argument('-d', action='store_true', help='Enable flag -d')

# Add the required unnamed arguments
parser.add_argument('file', help='Input file name')
parser.add_argument('output', help='Output file name')

# Parse the command-line arguments
args = parser.parse_args()

input_file = args.file
output_file = args.output


# Access the values of the flags and arguments
def compress(input_file, output_file):
    with open(input_file, 'r') as f:
        frequency_table = Counter(f.read())
        q = queue.PriorityQueue()
        for k, v in frequency_table.items():
            q.put((int(k), v))

        while q.qsize() > 1:
            left = q.get()
            right = q.get()
            total = int(left[1] + right[1])
            node = Node(None, total, left, right)
            pprint(vars(node))
            q.put((total, node))


if args.c:
    print("Flag -c is enabled")
    compress(input_file, output_file)

if args.d:
    print("Flag -d is enabled")
